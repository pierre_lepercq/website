from PIL import Image
import os

def convert_to_webp(source):
    to_convert = []
    if os.path.isdir(source):
        to_convert.extend([os.path.join(source, x) for x in os.listdir(source) if x.endswith(('.png', '.jpg'))])
    if os.path.isfile(source):
        to_convert.append(source)
    for filepath in to_convert:
        destination = "{}.webp".format(filepath.rsplit('.', 1)[0])
        with Image.open(filepath) as img:
            img.save(destination, quality=90)

if __name__ == "__main__":
    convert_to_webp('./resources')